import FormContainer from './Components/FormContainer.jsx'
import React, { Component } from 'react'
import { Container } from 'react-bootstrap'
import Header from './Components/Header'

export class App extends Component {
    state = {
        userInputs: [
            { inputType: 'text', label: 'Name', name: 'name' },
            { inputType: 'text', label: 'Email', name: 'email' },
            { inputType: 'password', label: 'Password', name: 'password' },
            {
                inputType: 'radio',
                label: 'Gender',
                name: 'gender',
                values: ['Male', 'Female'],
            },
            {
                inputType: 'checkbox',
                label: 'Vehicle',
                name: 'vehicle',
                values: ['Bike', 'Car'],
            },
            {
                inputType: 'dropdown',
                label: 'Vehicle Type',
                name: 'car',
                value: [
                    'Hatchback',
                    'Sedan',
                    'SUV',
                    'Road bikes',
                    'Mountain bikes',
                ],
            },
            {
                inputType: 'textarea',
                label: 'Address',
                name: 'address',
            },
        ],
    }

    render() {
        return (
            <Container>
                <Header />
                <div className="container-wrap ">
                    <div className="wrap-form-app">
                        <FormContainer userInputs={this.state.userInputs} />
                    </div>
                </div>
            </Container>
        )
    }
}

export default App
