import React, { Component } from 'react'
import { Form } from 'react-bootstrap'

export class DropdownMenu extends Component {
    render() {
        let dropdownStyle = {
            background: 'black',
            color: 'white',
        }
        let { value, label } = { ...this.props.inputs }
        console.log('drop ' + value + value)
        return (
            <React.Fragment>
                <Form.Group controlId="exampleForm.ControlSelect1">
                    <Form.Label>{label} </Form.Label>
                    <Form.Control
                        as="select"
                        style={dropdownStyle}
                        className="dropdown"
                    >
                        {value.map((item) => {
                            return (
                                <option key={item} style={dropdownStyle}>
                                    {item}
                                </option>
                            )
                        })}
                    </Form.Control>
                </Form.Group>
            </React.Fragment>
        )
    }
}

export default DropdownMenu
