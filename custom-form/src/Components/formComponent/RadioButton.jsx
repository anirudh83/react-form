import React, { Component } from 'react'
import { Form, ButtonGroup, Button } from 'react-bootstrap'
import '../Css/style.css'

export class RadioButton extends Component {
    render() {
        let { label, values } = { ...this.props.inputs }
        console.log('radio ' + values)
        return (
            <React.Fragment>
                <Form.Label style={{ marginRight: 15 }}>{label}</Form.Label>
                <ButtonGroup>
                    {values.map((value) => (
                        <Button active key={value}>
                            {value}
                            <input
                                type="radio"
                                name="radioButtonSet"
                                value={value}
                            />
                        </Button>
                    ))}
                </ButtonGroup>
            </React.Fragment>
        )
    }
}

export default RadioButton
