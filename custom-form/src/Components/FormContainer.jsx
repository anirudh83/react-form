import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import RadioButton from './formComponent/RadioButton'
import Checkbox from './formComponent/Checkbox'
import DropdownMenu from './formComponent/DropdownMenu'
import InputField from './formComponent/InputField'
import Textarea from './formComponent/Textarea'
import './Css/style.css'

export class FormContainer extends Component {
    state = {
        error: '',
    }
    handleSubmit = (event) => {
        try {
            console.log(event.target.address.value)
            if (
                event.target.name.value == '' ||
                event.target.email.value === '' ||
                event.target.password.value === '' ||
                event.target.radioButtonSet.value === '' ||
                event.target.address.value === '' ||
                (event.target.vehicle[0].checked === true ||
                    event.target.vehicle[1].checked === true) === false
            ) {
                this.setState({ error: 'Input fields can not be empty' })
                event.preventDefault()
            } else {
                this.setState({ error: '' })
            }
        } catch (err) {
            console.log(err)
        }
    }
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <p className="visible-on-top">{this.state.error}</p>

                {this.props.userInputs.map((row) => {
                    if (row.inputType === 'radio') {
                        return <RadioButton inputs={row} key={row.label} />
                    } else if (row.inputType === 'checkbox') {
                        return <Checkbox inputs={row} key={row.label} />
                    } else if (row.inputType === 'dropdown') {
                        return <DropdownMenu inputs={row} key={row.label} />
                    } else if (row.inputType === 'textarea') {
                        return <Textarea inputs={row} key={row.label} />
                    } else {
                        return <InputField input={row} key={row.label} />
                    }
                })}

                <Button variant="primary" type="submit" id="submit">
                    Submit
                </Button>
            </form>
        )
    }
}

export default FormContainer
