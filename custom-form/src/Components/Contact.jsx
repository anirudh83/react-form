import React, { Component } from 'react'
import locationImg from './img/location-icon-png-4225.png'
import './Css/style.css'

export class Contact extends Component {
    render() {
        return (
            <div className="container contact-wrap">
                <div className="row ">
                    <div className="col-sm contact-heading">Contact us</div>
                    <div className="col-sm contact-details">
                        For queries, suggestions and complaints to get in touch
                    </div>
                </div>
                <div className="row">
                    <div className=" address">
                        <img src={locationImg} alt="Address png" />
                        <h4>Address</h4>
                        <p>Roopena Nagar, Bangalore</p>
                    </div>
                    <div className=" phone">
                        <img
                            src="https://pngimg.com/uploads/phone/phone_PNG48988.png"
                            alt="Phone logo"
                        />
                        <h4>Phone</h4>
                        <p>+91-8382819293</p>
                    </div>
                    <div className="col-4 col-sm-4 col-md-4 email">
                        <img
                            src="https://smallimg.pngkey.com/png/small/90-904209_email-icon-android-application-icons-softiconsm-android-email.png"
                            alt="email logo"
                        />
                        <h4>Email</h4>
                        <p>ani@gmail.com</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Contact
